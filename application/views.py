import re
import io
import os
import re

from flask import render_template, send_from_directory
from flask import request, redirect
from flask_login import current_user, login_user, login_required, logout_user

from application.forms import *
from application.settings import db, app, BASE
from application.tree import LogMatrix
from .models import User, Pic, Sessions, make_convers


@app.route('/login', methods=['GET', 'POST'])
def login():
    graph = 'default.png'
    form = LoginForm(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.filter(User.login == form.username.data, User.password == form.password.data).first()
            if not user:
                return render_template('graph.html', form=form, message="Неверный логин или пароль.", default_graph=graph)
            login_user(user)
            return redirect('/')
    return render_template('graph.html', form=form, default_graph=graph)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect('/login')


@app.route('/register', methods=['GET', 'POST'])
def register():
    graph = 'default.png'
    form = RegistrationFrom(request.form)
    if request.method == 'POST':
        if form.validate_on_submit():
            if RegistrationFrom.correct_fields(request.form):
                isUser = User.query.filter(User.login == form.login.data).first()
                if not isUser:
                    if RegistrationFrom.validate_password(request.form):
                        user = User(form.login.data, form.password.data)
                        db.session.add(user)
                        db.session.commit()
                        return redirect('/login')
                return render_template('register.html', form=form, message='Данный логин занят.', default_graph=graph)
            return render_template('register.html', form=form, message='Не все поля заполнены корректно.', default_graph=graph)
    return render_template('register.html', form=form, default_graph=graph)


@app.route('/uploads/<filename>')
def send_file(filename):
    """
    Представление которое отдает файлы на закгрузку из директории media
    :param filename: 
    :return:  10.03 – 19.03 
    """
    return send_from_directory(os.path.join(BASE,'media'), filename)


@app.route('/results/<id>', methods=['GET', 'POST'])
@login_required
def show_graph(id):
    """
        Представление графа
        :param id:
        :return:
    """
    Graph = Pic.query.get(id)
    # Ненужная проверка
    # parser = re.findall(r'graph-\w+.png', Graph.pathPicCategory)
    # graphCat = [parser[0]]
    # parser = re.findall(r'graph-\w+.png', Graph.pathPicContent)
    # graphCont = [parser[0]]

    def get_keys(d):
        def get_keys_(d, res=set()):
            for key, val in d.items():
                if isinstance(val, dict):
                    res = get_keys_(val, res)
                res.add((key, key))
            return res
        return sorted(get_keys_(d))

    form_cat = ChoicesConvers()
    form_cat.dropboxStartCat.choices = get_keys(eval(Graph.countCategory))
    form_cat.dropboxFinishCat.choices = get_keys(eval(Graph.countCategory))

    form_con = ChoicesConvers()

    form_con.dropboxStartCat.choices = get_keys(eval(Graph.countContent))
    form_con.dropboxFinishCat.choices = get_keys(eval(Graph.countContent))

    return render_template('renrer_result.html',
                           graphs=[('category', Graph.pathPicCategory, eval(Graph.weightCat)),
                                   ('content', Graph.pathPicContent, eval(Graph.weightCont))],  # список графов
                           form_cat=form_cat,
                           form_con=form_con,)


@app.route('/process', methods=['POST'])
@login_required
def process_graph():
    """
        Обработка графа
        :return:
    """
    targs = {}


    user_pars = re.findall('\d', current_user.__str__())
    # Ненужно
    # picList = Pic.query.filter(Pic.user_id == user_pars[0]).all()


    x = request.values.get('data_list')
    y = re.findall('\.log', str(request.files.get('log', None)))

    # if x == "" and y.__len__() != 0:
    file = request.files.get('log', None)  # открывает log file
    stream = io.StringIO(file.stream.read().decode("UTF8"), newline=None)

    log = LogMatrix(stream.read(), BASE=BASE + '/media/')  # цепляем суперпарсер/анализатор логов

    graphCat = log.get_graph('category')  # строим графы
    graphCont = log.get_graph('content')
    graph = Pic.saveInDb(graphCat[1], graphCont[1], log.count_category.__str__(), log.count_content.__str__(), log.couters.__str__(), graphCat[2].__str__(), graphCont[2].__str__(), user_pars[0])  # сохраняем в бд

    for x in log.sessions:
        Sessions.addInDb((''.join(k.__str__() for k in log.sessions[x])),(''.join(log.sessions[x][k].__str__() for k in log.sessions[x])), graph.id)
    Sessions.commitInDb()

    return show_graph(graph.id)

    # return render_template('graph.html', default_graph=graph, pic_list=picList, form=LoginForm(), **targs)

@app.route('/', methods=['POST', 'GET'])
def main_page():
    return render_template('graph.html', default_graph='default.png', form=LoginForm())


@app.route('/convers/<type>', methods=['POST', 'GET'])
def view_convers(type):
    convers = 0
    Pics = Pic.query.order_by('-id').first()
    #count= eval(Pics.countCategory if type =='category' else Pics.countContent)
    #couters = eval(Pics.couters)
    try:
        convers = make_convers(Pics.id, request.values.get('dropboxStartCat'), request.values.get('dropboxFinishCat'), int(request.values.get('num')))
        #convers = (count[request.values.get('dropboxStartCat')][request.values.get('dropboxFinishCat')]/couters[request.values.get('dropboxFinishCat')])*100
        convers = round(convers, 2)
    except ZeroDivisionError:
        convers = "0"
    except TypeError:
        convers = convers
    return render_template('parts/convers.html', convers=convers)


if __name__ == '__main__':
    db.create_all()  # таблицы
    app.run()
