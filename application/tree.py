import random
import re
import string
from collections import namedtuple
import matplotlib.pyplot as plt
import time

import networkx



class LogMatrix:
    users = set()  # множество, тут собираем пользователей (ip)
    elements = set()
    content = set()

    count_category = dict()  # формат:
    # {"отсюда1": {"сода1": счетчик, "сюда2": счетчик, ..."сюдаN": счетчик, }, "отсюдаN": {...}} считаем переходы
    count_content = dict()
    # all_count = dict

    category_id = 1  # номер элемента (первый)
    content_id = -1  # номер элемента последний
    logfile = None  # файл лога

    sessions = dict() # словар с сессиями
    k = -1



    def print_(self):
        print("Категории: %s" % self.elements)
        print("Контент: %s" % self.content)
        print("users: %s" % self.users)

        print("count_content:")
        print(self.count_content)
        print("count_category:")
        print(self.count_category)
        print('all_count content')
        print(self.couters)

    def parse_log(self, logfile):
        """
        Генератор. преобразует строчку лога в LogNote Object c полями ['user', 'method', 'elems', 'status']
        :param logfile: 
        :return: 
        """
        LogNote = namedtuple('LogNole', ['user', 'method', 'elems', 'status'])
        # print(logfile)
        for line in logfile.split('\n'):
            if line == "":
                continue

            parsed = re.match('^(?P<user>\d+\.\d+\.\d+\.\d+).+\"(?P<method>GET|POST|HEAD|\w+)'
                              '\s(?P<path>\S+)\sHTTP/.+\"\s(?P<status>\d{3})', line)  # парсим строчку лога

            elems = parsed.group('path').split('?')[0].split('/')  # отрезаем GET параметры если они есть
            elems = [] if elems == ["", ""] else elems

            elems = ["MainPage"] + [x for x in elems if x != ""]  # добавляем фиктивный элелемент - корень

            yield LogNote(parsed.group('user'), parsed.group('method'), elems, parsed.group('status'))

    def __init__(self, raw_log_file, BASE):
        self.BASE = BASE
        def process_user_log(user_ip):
            """
            Воспроизведение действий пользователя в рамках одного лога
            :param user_ip: 
            :return: 
            """
            prev_point = None  # Предыдущее действие, по умолчанию отстутсвует



            for log_note in self.parse_log(raw_log_file):
                if log_note.user == user_ip and log_note.status == "200":  # только успешные запросы от интересующего нас юзера
                    if prev_point is None:
                        prev_point = log_note.elems
                        continue
                    self.k += 1
                    self.sessions.update({self.k:{log_note.user.__str__():', '.join(x for x in log_note.elems if x!= 'MainPage')}})
                    def check_counter_path(counter_dict, counter_id, prev_point):
                        """
                        считалка весов
                        :param counter_dict: 
                        :param counter_id: 
                        :return: 
                        """

                        if counter_id == self.content_id:  # извлечение расширения из контента

                            if '.' in prev_point[counter_id]:
                                prev_point[counter_id] = prev_point[counter_id].split(".")[-1]
                            else:
                                return

                            if '.' not in log_note.elems[counter_id]:
                                return
                            try:
                                if '.' in log_note.elems[counter_id]:
                                    log_note.elems[counter_id] = log_note.elems[counter_id].split(".")[-1]
                            except IndexError:
                                return

                        if len(prev_point) == 1:
                            counter_id = 0

                        user_pre_point = prev_point[counter_id]
                        if counter_dict.get(user_pre_point, None) is None:
                            counter_dict[user_pre_point] = {}

                        if len(log_note.elems) == 1:
                            counter_id = 0

                        if user_pre_point == log_note.elems[counter_id]:  # переход в эту-же категорию
                            return

                        if counter_dict[user_pre_point].get(log_note.elems[counter_id], None) is None:
                            counter_dict[user_pre_point][log_note.elems[counter_id]] = 0

                        try:
                            counter_dict[user_pre_point][log_note.elems[counter_id]] += 1
                        except KeyError:
                            print("create [%s]-> [%s] = %d" % (user_pre_point, log_note.elems[counter_id],
                                                               counter_dict[user_pre_point][
                                                                   log_note.elems[counter_id]]))
                            print("Error")

                    list(map(lambda x: check_counter_path(*x), [[self.count_category, self.category_id, prev_point],
                                                                [self.count_content, self.content_id, prev_point]]))
                    prev_point = log_note.elems

        for log_note in self.parse_log(raw_log_file):
            if log_note.method == "GET" and log_note.status == "200":
                # Анализируем только успешные GET запросы
                self.users.add(log_note.user)
                try:
                    self.elements.add(log_note.elems[self.category_id])  # первый элемент адреса
                    if '.' in log_note.elems[self.content_id]:
                        self.content.add(log_note.elems[self.content_id].split(".")[-1])  # последний элемент
                except IndexError:
                    pass
        list(map(process_user_log, self.users))

    def get_nodes(self, target=None):  # target = category или content
        """
        Возвращает узлы
        :param target: 
        :return: 
        """
        if target not in ['category', 'content']:
            return
        if target == 'category':
            return self.elements
        if target == 'content':
            return self.content

    @property
    def couters(self):
        result = {}
        "Возвращает полный список счетчиков"
        for target in [self.count_category,self.count_content]:
            for x,y,z in self.all_count(target):
                result[y] = result.get(y, 0) + z
        return result

    def all_count(self, target_dict):
        """
            Ждет на вход словарь: self.count_category или self.count_content
            возвращает переходы: (откуда, куда, кол-во)
        """
        def get_all_count(count_dict, y):
            count = 0
            for key, val in count_dict.items():
                count += val.get(y, 0)
            return count

        for x in list(target_dict):
            for y in list(target_dict[x]):
                yield (x, y, get_all_count(target_dict, y))

    def get_edges(self, target=None):
        """
        Возвращает связи с весами
        :param target: 
        :return: 
        """
        if target not in ['category', 'content']:
            return

        def result(target_dict):
            for x, y, z in self.all_count(target_dict):
                  yield (x, y, target_dict[x][y] / z)

        yield from result(self.count_category if target == 'category' else
                     self.count_content if target == 'content' else None)


    def make_convers(self): #функция для расчета конверсии
        dictConvers = {}
        sum_couters = 0
        proc = 0
        for x in self.couters:
            sum_couters += self.couters.get(x)
        for x in self.couters:
            dictConvers[x] = round(((self.couters.get(x)/sum_couters) * 100),2)

        return dictConvers

    def get_graph(self, target):
        G = networkx.MultiDiGraph()
        G.add_nodes_from(self.get_nodes(target))  # добавляем узлы
        G.add_weighted_edges_from(self.get_edges(target))  # добавляем связи
        # networkx.draw(G, with_graphCat=Pics.pathPicCategorylabels=True, node_size=500, font_size=10)
        pos = networkx.spring_layout(G)

        elarge = [(u, v) for (u, v, d) in G.edges(data=True)]

        networkx.draw_networkx_nodes(G, pos, node_size=500, )
        # edges
        networkx.draw_networkx_edges(G, pos, edgelist=elarge,
                                     width=1, arrows=True, style='dashed', edge_color='green', alpha=0.5)
        # networkx.draw_networkx_edges(G, pos, edgelist=esmall,
        #                        width=6, alpha=0.5, edge_color='b', style='dashed')
        # labels
        networkx.draw_networkx_labels(G, pos, font_size=6, font_family='sans-serif', )

        graph = 'graph-%s.png' % ''.join(
            random.choice(string.ascii_uppercase + string.digits) for _ in range(8))

        plt.axis('equal')
        plt.savefig(self.BASE + graph, format="PNG", dpi=250, width=10)
        plt.clf()

        #print(target)
        #print(G.edges(data=True))
        #print([(x[0], x[1], round(x[2]['weight'], 2)) for x in G.edges(data=True)])

        return target, graph, [(x[0], x[1], round(x[2]['weight'], 2)) for x in G.edges(data=True)]

#print(sum_couters)
 #       print(self.couters)
  #      print(dictConvers)
   #     for x in dictConvers:
    #        proc += dictConvers.get(x)
     #   print(proc)
# start = time.time()
# f = open('access.log', 'r')
# log = LogMatrix(f.read())
# log.print_()
# # print(log.get_nodes('content'))
# # print(log.get_edges('content'))
#
# print("time:")
# print(time.time()-start)

