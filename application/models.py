import datetime

from flask import json
from sqlalchemy.ext import mutable

from application.settings import db, login_manager

class Sessions(db.Model):
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    user = db.Column(db.Text)
    elems = db.Column(db.Text)
    pic_id = db.Column(db.Integer, db.ForeignKey('pic.id'))

    def __init__(self, user, elems, pic_id):
        self.user = user
        self.elems = elems
        self.pic_id = pic_id

    @classmethod
    def addInDb(cls, ip, elems, pic_id):
        cur_session = cls(ip, elems, pic_id)
        db.session.add(cur_session)

    @classmethod
    def commitInDb(cls):
        db.session.commit()

def make_convers(pic_id, A, B, N):
    allApoint = 0
    AandBpoints = 0
    s = Sessions.query.filter_by(pic_id=pic_id).all()
    for x in s:
        g = x.elems.split(', ')
        if A == g[0]:
            allApoint += 1
            for y in g:
                if B != g[0] and y == B:
                    AandBpoints += 1
    return round(AandBpoints/allApoint*N, 2)


class Pic(db.Model):  # модель таблицы для картинок
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True) # must have! для любой модели
    dateTimePic = db.Column(db.DateTime)
    pathPicCategory = db.Column(db.String(120), unique=True)
    pathPicContent = db.Column(db.String(120), unique=True)
    countCategory =db.Column(db.Text)
    countContent = db.Column(db.Text)
    couters = db.Column(db.Text)
    weightCat = db.Column(db.Text)
    weightCont = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    sessions = db.relationship('Sessions', backref="pic", lazy=True)

    def __init__(self, datetimepic, pathpiccategory, pathpiccontent, countcat, countcont, couters, cat, cont, user_id):
        self.dateTimePic = datetimepic
        self.pathPicCategory = pathpiccategory
        self.pathPicContent = pathpiccontent
        self.countCategory = countcat
        self.countContent = countcont
        self.couters = couters
        self.weightCat = cat
        self.weightCont = cont
        self.user_id = user_id

    def __str__(self):
        return '%s' % self.dateTimePic

    @classmethod
    def saveInDb(cls, pathcat, pathcont, countcat, countcont, couters, cat, cont, user_id):  # метод сохранения в бд
        pic = cls(datetime.datetime.now(), pathcat, pathcont, countcat, countcont, couters, cat, cont, user_id)
        db.session.add(pic)
        db.session.commit()
        return pic


class JsonEncodedDict(db.TypeDecorator):
    """Enables JSON storage by encoding and decoding on the fly."""
    impl = db.Text

    def process_bind_param(self, value, dialect):
        if value is None:
            return '{}'
        else:
            return json.dumps(value)

    def process_result_value(self, value, dialect):
        if value is None:
            return {}
        else:
            return json.loads(value)


mutable.MutableDict.associate_with(JsonEncodedDict)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(255))
    pics = db.relationship('Pic', backref="user", lazy=True)

    @property
    def pic_list(self):
        return Pic.query.filter(Pic.user_id == self.get_id()).all()

    def __init__(self, login, password):
        self.login = login
        self.password = password

    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    @staticmethod
    @login_manager.user_loader
    def load_user(userid):
        return User.query.get(int(userid))