import wtforms
from wtforms import validators


class LoginForm(wtforms.Form):
    username = wtforms.StringField()
    password = wtforms.PasswordField()
    validate_on_submit = wtforms.SubmitField('Вход')


class RegistrationFrom(wtforms.Form):
    login = wtforms.StringField()
    password = wtforms.PasswordField()
    conf_password = wtforms.PasswordField()
    validate_on_submit = wtforms.SubmitField('Принять')

    def validate_password(self):
        return self['password'] == self['conf_password']

    def correct_fields(self):
        if self['login'] != '' and self['password'] != '' and self['conf_password'] != '':
            return True
        return False

class ChoicesConvers(wtforms.Form):
    dropboxStartCat = wtforms.SelectField()
    dropboxFinishCat = wtforms.SelectField()
    dropboxStartCont = wtforms.SelectField()
    dropboxFinishCont = wtforms.SelectField()
    buttonCat = wtforms.SubmitField('Показать конверсию')
    buttonCont = wtforms.SubmitField('Показать конверсию')
    #numberUsers = wtforms.IntegerField(validators.any_of(1))
