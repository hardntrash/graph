click==6.7
cycler==0.10.0
decorator==4.0.11
Flask==0.12.2
itsdangerous==0.24
Jinja2==2.9.6
MarkupSafe==1.0
networkx==1.11
numpy==1.13.0
pyparsing==2.2.0
python-dateutil==2.6.0
pytz==2017.2
six==1.10.0
Werkzeug==0.12.2
flask_login == 0.4.1
WTForms == 2.1
flask_sqlalchemy==2.3.2
matplotlib==2.0.2