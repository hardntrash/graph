import os

from application import flask_app
from application.settings import db

if __name__ == '__main__':
    db.create_all()  # таблицы
    BASE = os.path.dirname(os.path.abspath(__file__))
    flask_app.run()
